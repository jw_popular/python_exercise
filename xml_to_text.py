import os
import sys
import time

from xml.dom import minidom
import xml.etree.cElementTree as ET

import configparser

data_path = "xml_data"

l = list()
ll = list()

def getCurTime():
    t = time.localtime()
    return t

def getXmlName(strIndex):

    # SKAFLZSBRFFFxxxx.XML
    
    strExt = 'XML'

    result = "SKAFLZSBRFFF" + strIndex + "." + strExt

    return result

def hextoint(e):
  return int("0x" + e[:8], 16)


for id in range(1, 20):
    file=getXmlName(str("%04d"%id))
    
    f_path = os.getcwd() + "/" + data_path + "/" + file

    if os.path.isfile(f_path):

        #outfile = f_path[:len(f_path)-3] + "txt"


        #f = open(outfile, "w")
        # parse an xml file by name
        file = minidom.parse(f_path)

        #use getElementsByTagName() to get tag
        models = file.getElementsByTagName('Record')
        #print(type(models))
        print(models.length)
        # one specific item attribute
        #print('model #2 attribute:')
        #print(models[1].attributes['NUID'].value)

        # all item attributes
        #print('\nAll attributes:')
        
        for elem in models:
            l.append(elem.attributes['STB_CA_SN'].value + "," + elem.attributes['NUID'].value)
            ll.append(elem.attributes['NUID'].value)
            #f.write(elem.attributes['STB_CA_SN'].value + "," + elem.attributes['NUID'].value)
            #print(int("0x" + elem.attributes['STB_CA_SN'].value, 16))
            #print(hextoint(elem.attributes['STB_CA_SN'].value + "," + elem.attributes['NUID'].value))
        #print(l)
        #l.sort(key=hextoint)
        #print(l)

        #for item in l:
        #    f.write(item + "\n")
        # one specific item's data
        #print('\nmodel #2 data:')
        #print(models[1].firstChild.data)
        #print(models[1].childNodes[0].data)

        # all items data
        #print('\nAll model data:')
        #for elem in models:
        #    print(elem.firstChild.data)
        #f.close()

        #break
    else:
        pass

print(len(ll))
s = set(ll)
print(len(s))

l.sort(key=hextoint)

f = open("./xml_data/xml_parsed.txt", "w")

for item in l:
    f.write(item + "\n")

f.close()
