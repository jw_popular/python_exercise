
import sys
import os

from tkinter import messagebox as msgbox

import struct

import time

cscKey = "./data/D990_C2100000_C210FFFF_01_01_OTP_0801_04F7_00000001.csc"


t_start = time.time()

if os.path.isfile(cscKey):
    pass
    #msgbox.showinfo("Info", "File is exist! (%s)"%(cscKey))
    #print("File is exist!")
else:
    msgbox.showerror("Error", "File is Not exist! (%s)"%(cscKey))
    exit(1)

for s in cscKey.split(sep='/'):
    #print("{0} {1}".format(s, s.find("csc")))
    if s.find("csc")>=0:
        strFileName = s.split(sep='.')[0]
        break

#print(strFileName)
lstField = strFileName.split(sep='_')

print("==============================================================")
print("\tSTBProvID:", lstField[0])
print("\tNUIDMin: %s, NUIDMax: %s"%(lstField[1], lstField[2]))
print("\tType: %s, Usage: %s"%(lstField[3], lstField[4]))
print("\tProvMeans:", lstField[5])
print("\tCSCDataVer: %s, CSADSetID: %s"%(lstField[6], lstField[7]))
print("\tCSCDataRevNumber:", lstField[8])
print("==============================================================")

#print(struct.calcsize('H'))
#print(struct.calcsize('I'))
#print(struct.calcsize('HI'))

with open(cscKey, "rb") as fp:
    (f_format,) = struct.unpack("!H", fp.read(2)) # '!' us using to reverse byte, means 0x0020 -> 0x2000 
    (f_crc,) = struct.unpack("!I", fp.read(4))
    print("\tFormat: 0x%04X\tCRC32: 0x%08X"%(f_format, f_crc))
    
    (headlen_tag,) = struct.unpack("B", fp.read(1))
    (headlen_tag_len,) = struct.unpack("!H", fp.read(2))
    (head_len,) = struct.unpack("!I", fp.read(4))
    parse_len = head_len-7

    print("\tHeader Length Tag: 0x%02X\tLen: %d (%d)"%(headlen_tag, headlen_tag_len, parse_len))
    print("\t\tHearder Len:%d"%head_len)

    while(parse_len>0):
        parse_len-=1
        (p_tag,) = struct.unpack("B", fp.read(1))

        parse_len-=2
        (p_tag_len,) = struct.unpack("!H", fp.read(2))

        parse_len-=p_tag_len
        data = fp.read(p_tag_len)

        if p_tag_len==2:
            (p_tag_val,) = struct.unpack("!H", data)
        elif p_tag_len==4:
            (p_tag_val,) = struct.unpack("!I", data)
        elif p_tag_len==1:
            (p_tag_val,) = struct.unpack("B", data)

        if p_tag==0x11:
            record_cnt = p_tag_val
            print("\tRecord count Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: %d"%p_tag_val)
        elif p_tag==0x12:
            record_len = p_tag_val
            print("\tRecord length Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%08X (%d)"%(p_tag_val, p_tag_val))
        elif p_tag==0x14:
            print("\tNUIDMin Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%08X"%p_tag_val)
        elif p_tag==0x15:
            print("\tNUIDMax Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%08X"%p_tag_val)
        elif p_tag==0x16:
            nuid_pos = p_tag_val
            print("\tNUID position Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%04X (%d)"%(p_tag_val, p_tag_val))
        elif p_tag==0x20:
            print("\tNOCS level Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%02X 0x%02X"%((p_tag_val>>8) & 0xFF, (p_tag_val) & 0xFF))
        elif p_tag==0x22:
            print("\tSTBProvID Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: %02X%02X%02X"%(int(data[0]), int(data[1]), int(data[2])))
        elif p_tag==0x24:
            print("\tChipset manufacturer name Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            str_name=""
            for c in range(p_tag_len):
                str_name="{0}{1}".format(str_name,"%c"%(int(data[c])&0xFF))
            print("\t\tValue: [%s]"%(str_name))
        elif p_tag==0x25:
            print("\tOperator name Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            str_name=""
            for c in range(p_tag_len):
                str_name="{0}{1}".format(str_name,"%c"%(int(data[c])&0xFF))
            print("\t\tValue: [%s]"%(str_name))
        elif p_tag==0x40:
            print("\tOTP CONFIG ID Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            print("\t\tValue: 0x%04X 0x%04X"%((p_tag_val>>16) & 0xFFFF, (p_tag_val) & 0xFFFF))
        else:
            #print("\tHeader Tag: 0x%02X\tLen: %d (%d)"%(p_tag, p_tag_len, parse_len))
            str_name=""
            cnt=0
            for c in range(p_tag_len):
                if cnt==0:
                    str_name="\t\tValue: {0}".format("0x%02X"%(int(data[c])&0xFF))
                elif cnt!=0 and cnt%16==0:
                    str_name="{0}\n\t\t{2}{1}".format(str_name,"0x%02X"%(int(data[c])&0xFF),"       ")
                else:
                    str_name="{0} {1}".format(str_name,"0x%02X"%(int(data[c])&0xFF))
                cnt+=1
            #print("%s"%(str_name))
            #(p_tag_val,) = struct.unpack("s", fp.read(p_tag_len))
            #print("\t\tValue: %s"%p_tag_val)
                            
    print("==============================================================")

    for read_record in range(record_cnt):
        (p_record_val,) = struct.unpack("!I", fp.read(nuid_pos))
        if p_record_val == record_len:
            pass
        else:
            print("NUID position is wrong!!")
        (p_record_val,) = struct.unpack("!I", fp.read(4))
        print("%08X"%(p_record_val), end=" ")

        data = fp.read(record_len-nuid_pos-4)

        if (read_record+1)%16==0 or read_record==(record_cnt-1):
            print("")

print("==============================================================")
print("%.3f Seconds"%(time.time()-t_start))
print("==============================================================")
