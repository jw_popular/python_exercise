
'''
<serialnumberird>CB0AF2NUIDZ</serialnumberird>
<serialnumbersc>00uVA</serialnumbersc>
<rid>00NUID</rid>

'''

import os
import sys
import time

from xml.dom import minidom
import xml.etree.cElementTree as ET


asn_file = os.getcwd() + "/data/CO_SKED_LH02-303_NA_202211300000_22u.asn"

po_name = 'DUMMYP00003'

ird_prefix = "CB0AF2"

scale = 16 ## equals to hexadecimal

num_of_digits = 12

######
def calc_ReleaseL(inputStr):
    #print(inputStr)
    if len(inputStr)!=16:
        return ""

    int_ch = []
    for ich in inputStr:
        int_ch.append(int(ich, scale))
    #print(int_ch)

    temp = []
    for i in range(len(int_ch)):
        if i==0:
            for j in range(4):
                temp.append(int_ch[i])
        else:
            temp[0] ^= int_ch[i]
            if i<8:
                temp[1] ^= int_ch[i]
            if i not in (4, 5, 6, 7, 12, 13, 14, 15):
                temp[2] ^= int_ch[i]
            if i not in (2, 3, 6, 7, 10, 11, 14, 15):
                temp[3] ^= int_ch[i]

    #print(temp)   

    c = []
    for iTemp in temp:
        strTemp = bin(iTemp)[2:].zfill(4)
        #print(strTemp)
        c.append(int(strTemp[0],2) ^ int(strTemp[1],2) ^ int(strTemp[2],2) ^ int(strTemp[3],2))

    #print(c)

    check_digit = format(((c[3]<<3) + (c[2]<<2) + (c[1]<<1) + c[0]), 'X')

    return check_digit       

def getCurTime():
    t = time.localtime()
    return t

def getXmlName(strTime, strPO):
    strCountry = 'BR'
    strManuf = 'SKED'
    strModel = 'SH02-303'
    
    strExt = 'xml'

    result = strCountry + "_" + strManuf + "_" + strModel + "_" + strPO + "_" + strTime + "." + strExt

    return result

def indent(elem, level=0): # append newline and tab character for XML
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
######


# main codes
if os.path.isfile(asn_file):
    #print("File is exist!")
    pass
else:
    print("It is not exist!!")
    exit(1)

t = getCurTime()
runDate = time.strftime("%Y%m%d", t) 
runTime = time.strftime("%H%M%S", t) 

print(runDate + runTime)
output_xml_file = getXmlName(runDate+runTime, po_name)
print(output_xml_file)

ff = open(asn_file, "r", encoding="utf-8")

ff.seek(0)
lst = ff.readlines()
print("Total %d lines.\n"%len(lst))

if len(lst)>0:
    
    root = ET.Element("palletid")

    ### XML Header ###
    '''
    <header>
        <manufacturerid>SKARDIN</manufacturerid>
        <typeproperty>C</typeproperty>
        <invoice>00003</invoice>
        <invoiceserie>003</invoiceserie>
        <creationdate>20221121</creationdate>
        <creationtime>150800</creationtime>
        <equipamenttype>N</equipamenttype>
    </header>
    '''
    header = ET.SubElement(root, "header")
    ET.SubElement(header, "manufacturerid").text="SKARDIN"
    ET.SubElement(header, "typeproperty").text="C"
    ET.SubElement(header, "invoice").text="00003"
    ET.SubElement(header, "invoiceserie").text="003"
    ET.SubElement(header, "creationdate").text=runDate
    ET.SubElement(header, "creationtime").text=runTime
    ET.SubElement(header, "equipamenttype").text="N"
    ##################

    ### XML Detail ###
    '''
    <detail id="1" partnumber="2654052427" palletid="SKED-000000001" materialcode="605572">
    '''
    detail = ET.SubElement(root, "detail", id="1", partnumber="2654052427", palletid="SKED-000000001", materialcode="605572")
    ###################
    ### XML Detailline ###
    '''
    <detailline>
      <serialnumberird>CB0AF23255832388Z</serialnumberird>
      <serialnumbersc>002552037430</serialnumbersc>
      <rid>003255832388</rid>
    </detailline>
    '''
    for ss in lst:
        detailline = ET.SubElement(detail, "detailline")
        #print("-->" + ss.strip() + "<--\n")
        str_vUA = ss.strip().split(sep="~")[6]
        str_nuid = ss.strip().split(sep="~")[12]
        #print(str_vUA, str_nuid)

        chk = calc_ReleaseL(ird_prefix + str_nuid[2:])
        if chk=="":
            print("Error Input:", str_vUA, str_nuid)
        else:
            #print(ird_prefix + str_nuid[2:] + chk)
            #print(bin(int(iStr, scale))[2:].zfill(len(iStr)*4))
            
            ET.SubElement(detailline, "serialnumberird").text= (ird_prefix + str_nuid[2:] + chk)
            ET.SubElement(detailline, "serialnumbersc").text= str_vUA
            ET.SubElement(detailline, "rid").text= str_nuid
            pass

    #######################


    tree = ET.ElementTree(root)
    indent(root)
    tree.write(output_xml_file, xml_declaration=True, encoding="UTF-8", method="xml")


ff.close()