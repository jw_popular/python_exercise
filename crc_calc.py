import binascii
def computeFileCRC(filename):
    try:
        blocksize = 1024 * 64
        f = open(filename, "rb")
        f.read(16)
        str = f.read(blocksize)
        crc = 0
        while len(str) != 0:
            crc = binascii.crc32(str,crc) & 0xffffffff
            str = f.read(blocksize)
        f.close()
    except:
        print("compute file crc failed!")
        return 0
    return crc

filename = "./data/00_02_5f_09_key_pair"

print ("crc32: %08X" % computeFileCRC(filename))

