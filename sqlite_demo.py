# -*- coding: utf-8 -*-
import sqlite3

print("SQLite Version:", sqlite3.version)

db = sqlite3.connect('sqlite_demo.db')
sql_query = '''SELECT name FROM sqlite_master WHERE type='table';'''
#sql_query = '''SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%';'''
#sql_query = '''SELECT name FROM sqlite_master;'''
#sql_query = '''SELECT name FROM sqlite_schema;'''

cursor = db.cursor()
print('Connect ok')

cursor.execute(sql_query)
#print(cursor.fetchone())
row = cursor.fetchall()

table_num = len(row)
if table_num==0:
    # Create table
    cursor.execute('''CREATE TABLE HUMAN (ID INT PRIMARY KEY NOT NULL, NAME TEXT NOT NULL, AGE INT NOT NULL);''')
    db.commit()
    print('Table created.')
    db.close()
    exit()
else:
    print(len(row), row)

    results = cursor.execute('''SELECT * FROM HUMAN''')
    row = cursor.fetchall()

    id = len(row)
    id += 1
    sql_query = '''INSERT INTO HUMAN (ID,NAME,AGE) VALUES (%d, 'Clay', 25)'''%(id)
    # Insert
    cursor.execute(sql_query)

    id += 1
    sql_query = '''INSERT INTO HUMAN (ID,NAME,AGE) VALUES (%d, 'Clay', 25)'''%(id)
    cursor.execute(sql_query)
    db.commit()
    print('Insert ok')
    
db.close()

# ---------------------------------------------------------------------------------

db = sqlite3.connect('sqlite_demo.db')
cursor = db.cursor()

# Select
results = cursor.execute('''SELECT * FROM HUMAN''')
row = cursor.fetchall()
#for item in results:
#    print(item)
print(row)

# Update
results = cursor.execute('''UPDATE HUMAN set AGE = 26 WHERE ID = 1''')
db.commit()
print('Update ok')

# Select
results = cursor.execute('''SELECT * FROM HUMAN''')
for item in results:
    print(item)

# Delete
cursor.execute('''DELETE FROM HUMAN WHERE ID = 2''')
db.commit()
print('Delete ok')

# Select
results = cursor.execute('''SELECT * FROM HUMAN''')
for item in results:
    print(item)

# Delete
cursor.execute('''DELETE FROM HUMAN WHERE ID = 1''')
db.commit()
print('Delete ok')

db.close()