import os
import sys

curPath = "/AEL025100/example/"

fName = os.getcwd() + curPath + "test_file.txt"
pName = os.getcwd() + "/data/"

print(os.curdir)
print(os.getcwd())

if os.path.isdir(pName):
    print(f"{pName}--------")
    try:
        os.rmdir(pName)
        os.mkdir(pName)
    except Exception as e:
        print(e)
    finally:
        print("mkdir done!")
else:
    print(f"!!!!!!!!{pName}")
    os.mkdir(pName)

if os.path.isfile(fName):
    print("File is exist!")
else:
    print("It is not exist!!")

if os.path.exists(fName):
    print("File is exist!")
else:
    print("It is not exist!!")

ff = open(fName, "r", encoding="utf-8")


with open(pName + "/test_test.txt", "w") as fp:
    fp.write("test123\n")
    fp.write("1===================\n")
    fp.write(ff.read()+"\n")
    fp.write("2===================\n")
    ff.seek(0)
    fp.write(ff.readline(15)+"\n")
    fp.write("3===================\n")
    ff.seek(0)
    lst = ff.readlines()
    fp.write("Total %d lines.\n"%len(lst))
    for ss in lst:
        fp.write("-->" + ss.strip() + "<--\n")
    fp.write("4===================\n")
    fp.flush()

ff.close()

if fp.closed:
    print("test_test.txt is closed")
else:
    print("Have to close test_test.txt")
    fp.close()

if ff.closed:
    print("test_file.txt is closed")
else:
    print("Have to close test_file.txt")
    ff.close()

