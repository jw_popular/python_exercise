tuple1 = ('東','南','西')
print(tuple1)
East,South,West = tuple1
print(South)
tuple2 = tuple1 + ('北',) 
print(tuple2)
tuple1,tuple2 = tuple2,tuple1
print(tuple1)
print(tuple2)
print(len(tuple1))
del(tuple2)
#print(tuple2) #
list1 = list(tuple1)
list1.append('東北')
print(list1)
tuple1 = tuple(list1)
print(tuple1)
print(tuple1[0])
print('東北' in tuple1)
for t in tuple1:
    print(t, end=',')
