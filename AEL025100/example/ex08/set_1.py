set1 = {'Anastasia'} 
print(set1)  #{'Anastasia'}
set1 = set('Anastasia')
print(set1)  #{'n', 't', 'i', 'a', 'A', 's'}
set1 = set({'貓':'cat','狗':'dog'})
print(set1)  #{'貓', '狗'}
set1 = set('嘻嘻哈哈')
print(set1)  
set1.add('笑嘻嘻')
print(set1)  
set1.remove('笑嘻嘻')
print(set1)  
set1.discard('笑嘻嘻')
set1.update('笑嘻嘻')
print(set1) 
#set1.remove('笑嘻嘻')
set1.pop()
print(set1)
