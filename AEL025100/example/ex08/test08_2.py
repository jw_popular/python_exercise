import datetime
dSpc=('德州炸雞','起司漢堡','海鮮寬麵','鮮蔬義麵')
wSpc=('海鮮總匯','超級拼盤','披薩雙拼')
nowDate=datetime.datetime.now()
today=nowDate.strftime('%A')
print(today)
print("創意餐廳菜單")
if today in ("Friday","Saturday","Sunday"):
    print("周末特餐：")
    for menu in wSpc:
        print(menu)
else:
    print("平日特餐：")
    for menu in dSpc:
        print(menu)
days=6-nowDate.weekday()
print(f"下周平日特餐 {days} 天後推出")