# str.format()
# After Python 2.6 support

s1 = 'ABCDEFGHIJKLMNOP'

s2 = 1280

s3 = {'title':'SuperStar','price':1450}

s4 = ["Macallan", 1550]

print("Item: {1}, Price: {0}".format(s2, s1))

print("Item: {1}, Price1: {0}, Price2: {0}".format(s2, s1))

print("Item: {item_name}, Price1: {item_price}, Price2: {item_price}".format(item_name = s1, item_price = s2))

print("Item: {title}, Price1: {price}, Price2: {price}".format(**s3))

print("Item: {0[0]}, Price1: {0[1]}, Price2: {0[1]}".format(s4))

"""
Item: ABCDEFGHIJKLMNOP, Price: 1280
Item: ABCDEFGHIJKLMNOP, Price1: 1280, Price2: 1280
Item: ABCDEFGHIJKLMNOP, Price1: 1280, Price2: 1280
Item: SuperStar, Price1: 1450, Price2: 1450
Item: Macallan, Price1: 1550, Price2: 1550
"""

'''
Item: ABCDEFGHIJKLMNOP, Price: 1280
Item: ABCDEFGHIJKLMNOP, Price1: 1280, Price2: 1280
Item: ABCDEFGHIJKLMNOP, Price1: 1280, Price2: 1280
Item: SuperStar, Price1: 1450, Price2: 1450
Item: Macallan, Price1: 1550, Price2: 1550
'''