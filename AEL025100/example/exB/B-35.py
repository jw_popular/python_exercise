import datetime
dailySpecials = ("義大利麵","通心麵","烘肉卷","烤雞")
weekendSpecials = ("龍蝦","排骨","鱈魚")
now = datetime.datetime.now()
today = now.strftime("%A")
print("健康食物外送")
if today == "Friday" or today == "Saturday" or today == "Sunday":
    print("每週末特價食物項目:")
    for item in weekendSpecials:
        print(item)
else:
    print("每天特價食物項目:")
    for item in dailySpecials:
        print(item)
daysLeft = 6 - now.weekday()
print(f"本週尚剩餘 {daysLeft} 天")