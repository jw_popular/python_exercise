def cube(num) :
    """ Returns the cube of number n """
    return num*num*num

print(cube.__doc__)

