import os
def firstLine(fName, mode):
    if os.path.isfile(fName):
        with open(fName, 'r') as file:
             return file.readline()
    else:
        return None

print(firstLine("fruit.txt", 'r'))