onTime = input("單車是否在晚上 10 點前返還？(請填 y 或 n)").lower()
days = int(input("請輸入單車出租天數？"))
weekday = input("單車是在星期幾出租?(請用英文)").capitalize()
money = 100
if onTime  == "n":
   days += 1
if weekday == "Sunday":
   total = (days * money) * 0.8 
elif weekday == "Wednesday": 
   total = (days * money) * 0.6
else:
   total = (days * money) 
print("單車的出租費用總計為 : ", int(total), "元")
