import tkinter as tk

win = tk.Tk()

win.title('測試視窗')
win.resizable(False, False)

window_height = 600
window_width = 600
screen_width = win.winfo_screenwidth()
screen_height = win.winfo_screenheight()

x_cordinate = int((screen_width/2) - (window_width/2))
y_cordinate = int((screen_height/2) - (window_height/2))

win.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))

#win.geometry('600x600')

lbl01 = tk.Label(win, text='Label Title 中文', bg='pink', font=('DFKai-SB',16))
lbl02 = tk.Label(win, text='123 Label Title 中文', bg='yellow', fg='red', font=('DFKai-SB',24))
lbl03 = tk.Label(win, text='中文 123 中文', width=15, height=2, padx=20, pady=5, bg='lightgray', fg='blue', font=('DFKai-SB',24))
lbl04 = tk.Label(win, text='Label Title 中文', bg='pink', font=('DFKai-SB',16))

lbl01.pack(side='top')
lbl04.pack(side='bottom')
lbl02.pack(side='left')
lbl03.pack(side='right')

win.mainloop()