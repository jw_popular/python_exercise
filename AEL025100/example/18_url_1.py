import urllib.parse
import urllib.request

urlstr = "https://www.google.com/search?q=python&rlz=1C1CHZN_zh-TWTW991TW991&oq=python&aqs=chrome..69i57j69i59l4j69i60l3.1687j0j15&sourceid=chrome&ie=UTF-8"
urlstr1 = "https://www.google.com/"

resultObj = urllib.parse.urlparse(urlstr)

resultObj1 = urllib.request.urlopen(urlstr1)


print("Parse Result:", resultObj)

print("fragment:", resultObj.fragment)
print("netloc:", resultObj.netloc)
print("path:", resultObj.path)
print("params:", resultObj.params)
print("Port:", resultObj.port)
print("Scheme:", resultObj.scheme)
print("query:", resultObj.query)

print("====================================================")

print("url:", resultObj1.geturl())
print("header:\n", resultObj1.getheaders())
print("====================================================")
htmlcontent = resultObj1.read()

print(htmlcontent.decode())
print("====================================================")

print("status:", resultObj1.status)


