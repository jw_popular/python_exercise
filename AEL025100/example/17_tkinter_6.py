import tkinter as tk
from tkinter import messagebox as msgbox # for message box

from PIL import ImageTk, Image # for Image

lstEdu = ['國小', '國中', "高中", "大學", "研究所"]

def fnHello():
    pass

def fnClear():
    pass

def fnOk():
    pass

def fnShowSelect():
    pass

def fnShowSelect2():
    print("{0} {1} {2} {3} {4}".format(isEduCheck[0].get(), isEduCheck[1].get(), isEduCheck[2].get(), isEduCheck[3].get(), isEduCheck[4].get()))


win = tk.Tk()

win.title('checkbutton 範例')

#win.geometry('600x400')

isEduCheck = {}

for i in range(len(lstEdu)):
    isEduCheck[i] = tk.BooleanVar()
    #tk.Checkbutton(win, text=lstEdu[i], variable=isEduCheck[i], command=fnShowSelect2, font=('DFKai-SB',16)).grid(row=1, column=i)
    tk.Checkbutton(win, text=lstEdu[i], variable=isEduCheck[i], command=fnShowSelect2, font=('DFKai-SB',16)).pack()

bookImg = ImageTk.PhotoImage(Image.open("./data/AEL025100.jpg"))

#tk.Label(win, image=bookImg).grid(row=2)
tk.Label(win, image=bookImg).pack()

win.mainloop()