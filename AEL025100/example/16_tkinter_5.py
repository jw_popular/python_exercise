import tkinter as tk
from tkinter import messagebox as msgbox # for message box

'''
icon= error | info | question | warning
askokcancel() : True | False
askquestion() : 'yes' | 'no'
askretrycancel() : True | False
askyesno() : True | False
showerror()
showinfo()
showwarning()
'''
lstEdu = ['國小', '國中', "高中", "大學", "研究所"]
def fnHello():
    pass

def fnClear():
    pass

def fnOk():
    pass

def fnShowSelect():
    print(gender.get())

def fnShowSelect2():
    print("{0} {1}".format(edu.get(), lstEdu[edu.get()]))


win = tk.Tk()

win.title('radiobutton 範例')

win.geometry('600x400')

gender = tk.IntVar()
edu = tk.IntVar()
stredu = tk.StringVar()

gender.set(0)
radGenM = tk.Radiobutton(win, text='男', command=fnShowSelect, variable=gender, value=0, font=('DFKai-SB',16))
radGenF = tk.Radiobutton(win, text='女', command=fnShowSelect, variable=gender, value=1, font=('DFKai-SB',16))

radGenM.grid(row=0, column=0)
radGenF.grid(row=0, column=1)

for i in range(len(lstEdu)):
    tk.Radiobutton(win, text=lstEdu[i], variable=edu, command=fnShowSelect2, value=i, font=('DFKai-SB',16)).grid(row=1, column=i)

edu.set(3)

win.mainloop()