import matplotlib.pyplot as plt

listX = [2016, 2022]
listY = [0, 100000]

plt.plot(listX, listY, color='blue', ls='-.', lw=4, label="Sales volume")
plt.legend()
plt.show()