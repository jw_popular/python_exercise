import matplotlib.pyplot as plt
font = {'family' : 'DFKai-SB'}  
plt.rc('font', **font) 

listIYearX = [2017, 2018, 2019, 2020, 2021, 2022]

listIPhoneY = [43000, 31000, 70500, 68000, 85000, 24000]
plt.plot(listIYearX, listIPhoneY, color='blue', ls='-',  lw=2, 
         marker="o",ms=10, label="iPhone")

listAsusY = [23000, 36000, 40500, 58000, 65000, 44000]
plt.plot(listIYearX, listAsusY, color='red', ls='-.', lw=2,
         marker="*",ms=10,  label="ASUS")

listGoogleY = [13000, 26000, 50500, 68000, 75000, 54000]
plt.plot(listIYearX, listGoogleY, color='green', ls='-', lw=2, 
         marker="s",ms=10, label="Google")

plt.title("手機歷年銷售量")
plt.xlim(2016, 2023)
plt.ylim(0, 110000)
plt.xlabel('年度')
plt.ylabel('銷售量')
plt.legend()
plt.show()