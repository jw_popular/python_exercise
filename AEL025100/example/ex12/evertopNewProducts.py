import os
import requests
from bs4 import BeautifulSoup

pageName='index.html'   #指定網頁名稱

#捉取長峰資訊網頁
urlstr="http://www.evertop.com.tw"  #長峰資訊網址
responseObj=requests.get(urlstr)
responseObj.encoding='utf-8'
bs=BeautifulSoup(responseObj.text, 'html.parser')

#取得產品新訊的HTML區塊
data=bs.select(".mb")[1]  

#將產品新訊的圖檔位址放入imgSrc串列
img=data.select('img')
imgSrc=[]
for n in range(len(img)):   
    imgSrc.append(img[n].get('src'))

#將產品新訊的標題放入linkText串列
link=data.select('.mtitle a')
linkText=[]
for n in range(len(link)):  
    linkText.append(link[n].text.strip())

#建立index.html網頁
f=open(pageName,'w', encoding='utf-8')
#寫入HTML進行編排網頁
f.write('<html>')
f.write('<head>')
f.write('<meta charset="utf-8">')
f.write('<title>長峰資訊</title>')
f.write('</haed>')
f.write('<body>')
f.write('<h2 align="center">產品新訊</h2>')
#使用迴圈配合HTML、linkText、imgSrc串列編排網頁區塊
for n in range(len(imgSrc)):
    f.write('<div style="float:left;width:400px;height:250px;margin:10px;background-color:#E8FFE8;text-align:center">')
    f.write('<img src="%s%s" width="300"><br>' %(urlstr, imgSrc[n]))
    f.write(linkText[n])
    f.write('</div>')
f.write('</body>')
f.write('</html>')
f.close()
os.system(pageName)  #開啟index.html網頁

