import tkinter as tk
from tkinter import messagebox as msgbox # for message box

'''
icon= error | info | question | warning
askokcancel() : True | False
askquestion() : 'yes' | 'no'
askretrycancel() : True | False
askyesno() : True | False
showerror()
showinfo()
showwarning()
'''
def fnHello():
    msgboxAns = msgbox.askquestion("Send Score", "請問?", icon='question')
    
    if msgboxAns=='yes':
        vName = name.get()
        vScore = score.get()
        lblShow['text']="The score of %s is %.1f"%(vName, vScore)
    elif msgboxAns=='no':
        lblShow['text']="Ignore 成績 deliver"
    else:
        lblShow['text']="Doesn't make sense return by askquestion()"


def fnClear():
    msgboxAns = msgbox.askyesno("Send Score", "請問?", icon='error')

    if msgboxAns:
        lblShow['text']=""
        lblShow1['text']=""
    else:
        pass

def fnOk():
    
    vAge = age.get()
    vMarried = isMarry.get()

    if vMarried:
        vStrMarry="married"
    else:
        vStrMarry="non-married"
    lblShow1['text']="%d years old and %s"%(vAge, vStrMarry)

win = tk.Tk()

win.title('Entry 範例')

win.geometry('600x400')

name = tk.StringVar()
score = tk.DoubleVar()
age = tk.IntVar()
isMarry = tk.BooleanVar()

lblName = tk.Label(win, text='Name', width=10, font=('DFKai-SB',16))
lblAge = tk.Label(win, text='Age', width=10, font=('DFKai-SB',16))
lblScore = tk.Label(win, text='Score', width=10, font=('DFKai-SB',16))
lblMarry = tk.Label(win, text='Married', width=10, font=('DFKai-SB',16))

txtName = tk.Entry(win, width=20, textvariable=name, font=('DFKai-SB',16))
txtAge = tk.Entry(win, width=20, textvariable=age, font=('DFKai-SB',16))
txtScore = tk.Entry(win, width=20, textvariable=score, font=('DFKai-SB',16))
txtMarry = tk.Entry(win, width=20, textvariable=isMarry, font=('DFKai-SB',16))

btnHello = tk.Button(win, text='Hello', width=10, command=fnHello, underline=2, font=('DFKai-SB',16))
btnOk = tk.Button(win, text='OK', width=10, command=fnOk, font=('DFKai-SB',16))
btnClear = tk.Button(win, text='Clear', width=10, command=fnClear, font=('DFKai-SB',16))

lblShow = tk.Label(win, text='', width=30, font=('DFKai-SB',16))
lblShow1 = tk.Label(win, text='', width=30, bg='pink', fg='blue', font=('DFKai-SB',16))

lblName.grid(row=0, column=0)
lblAge.grid(row=1, column=0)
lblScore.grid(row=2, column=0)
lblMarry.grid(row=3, column=0)

txtName.grid(row=0, column=1)
txtAge.grid(row=1, column=1)
txtScore.grid(row=2, column=1)
txtMarry.grid(row=3, column=1)

btnHello.grid(row=4, column=0)
btnOk.grid(row=4, column=1)
btnClear.grid(row=4, column=2)

lblShow.grid(row=5, column=1)
lblShow1.grid(row=6, column=1)

# Below codes will cause error
# _tkinter.TclError: cannot use geometry manager pack inside . which already has slaves managed by grid
#lblShow.pack()
#lblShow1.pack()

win.mainloop()