import tkinter as tk
from tkinter import font

def setWindow(win):
    win.title("Listbox & Scrollbar test")
    win.resizable(False, False)
    screen_width = win.winfo_screenwidth()
    screen_height = win.winfo_screenheight()
    x_cordinate = int((screen_width/2) - (window_width/2))
    y_cordinate = int((screen_height/2) - (window_height/2))
    win.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))

def button_event():
    # print(type(mylistbox.curselection()))
    lst = listbox.curselection()
    print(type(lst))
    print(lst)
    for i in range(len(lst)):
        print(listbox.get(lst[i]))

def listbox_event(event):
    object = event.widget
    # print(type(object.curselection()))
    print(object.curselection())
    index = object.curselection()[0]
    mylabel.configure(text=object.get(index))


window_height = 600
window_width = 600

root = tk.Tk()

setWindow(root)

frameA = tk.Frame(master=root, bg="blue")
frameA.place(x=0, y=0, width=300, height=100)

frameB = tk.Frame(master=root, bg="yellow")
frameB.place(x=300, y=0, width=300, height=100)

frame1 = tk.Frame(master=root, bg="green")
frame1.pack(side=tk.BOTTOM, fill=tk.X)

scrollbar = tk.Scrollbar(frame1)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

# selectmode= tk.BROWSE | tk.SINGLE | tk.MULTIPLE | tk.EXTENDED
listbox = tk.Listbox(frame1, selectmode=tk.EXTENDED, yscrollcommand=scrollbar.set, font=('DFKai-SB',14))

for i in range(100):
    listbox.insert(i, "#"+str(i))

listbox.bind("<<ListboxSelect>>", listbox_event)
#listbox.pack(side=tk.BOTTOM, fill=tk.BOTH)
listbox.pack(fill=tk.X)

scrollbar.config(command=listbox.yview)

mylabel = tk.Label(root, font=('DFKai-SB',14))
mylabel.pack()

tk.Button(root, text='Selected', command=button_event, font=('DFKai-SB',14)).pack()

root.mainloop()

