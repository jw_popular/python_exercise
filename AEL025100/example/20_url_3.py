import requests
from bs4 import BeautifulSoup

urlstr = "https://www.imdb.com/chart/top/"

resultObj = requests.get(urlstr)

print("url:", resultObj.url)
print("header:\n", resultObj.headers)
print("====================================================")
resultObj.encoding='utf-8'

#print(resultObj.text)

bs = BeautifulSoup(resultObj.text, 'html.parser')

print(bs.title.text)

data = bs.select("The Shawshank Redemption")

print("====================================================")

print(data)

print("status:", resultObj.status_code)


