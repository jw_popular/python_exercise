# -*- coding: utf-8 -*-
money = int(input('請輸入存款金額：'))
rate = float(input('請輸入年利率：'))
years = int(input('請輸入年數：'))
print(f'{money:,}元年利率{rate:.2f}% \n{years:3}年', end='')
print(f'本利和為{money+money*rate/100*years:09,}元')
