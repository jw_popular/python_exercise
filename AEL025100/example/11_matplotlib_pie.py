
import matplotlib.pyplot as plt

# For Chinese font
font = {'family' : 'DFKai-SB'}
plt.rc('font', **font)

'''
explode
shadow
labeldistance
pctdistance
autopct: %m.nf%
startangle
'''
lstPct = [15.5, 18, 34.5, 7, 25]

lstExplode = [0, 0, 0, 0, 0]

if sum(lstPct)!=100.0:
    print("The sum of list is not 100")
    exit(1)

id=0
for i in range(len(lstPct)):
    if lstPct[i]==max(lstPct):
        id = i
        break

lstExplode[i] = 0.3

lstBook = ["ABC", "A1B2", "CDE", "XYZ", "GOOD"]
lstColors = ["red", "green", "blue", "purple", "yellow"]

plt.pie(lstPct, colors=lstColors, labels=lstBook, explode=lstExplode, shadow=False, labeldistance=1.1, pctdistance=0.5, autopct='%3.1f%%', startangle=0)

#plt.axis('equal')
plt.legend() # for label 
plt.show()

