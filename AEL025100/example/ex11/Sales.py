import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import messagebox as msgbox

font = {'family' : 'DFKai-SB'}  
plt.rc('font', **font)

#定義fnOk函式，當按下確定鈕會執行此函式
def fnOk():
    # 取得遊戲的value值，即代表選取該取方塊第幾個項目
    index = game.get();
    # 將遊戲名稱與銷售數量放入串列中
    if gameAry[index] in SalesGameAry:   # 判斷遊戲名稱是否在串列中
        # 若遊戲名稱重複指定則找出銷售量串列的索引位置，並累加銷售量
        n=SalesGameAry.index(gameAry[index])
        SalesNumAry[index] += num.get() 
    else:                               # 將遊戲名稱與銷售量放入串列
       SalesGameAry.append(gameAry[index])
       SalesNumAry.append(num.get())   
    # 將結果顯示指定result變數
    result = '{0}的訂購數量{1}'.format(gameAry[index], num.get())
    msgbox.showinfo('訂單結果', result)
    # 繪製遊戲銷售量的柱狀圖
    plt.bar(SalesGameAry, SalesNumAry, color='blue', label="遊戲銷售量")
    plt.title("遊戲銷售量")
    plt.xlabel('遊戲名稱')
    plt.ylabel('銷售量')
    plt.legend()
    plt.grid(True)
    plt.show()

win = tk.Tk()
win.title('遊戲銷售統計')
win.geometry('450x150')
# 指定tkinter模組的整數物件game，是火影忍者, 航海王, 人中之龍, 瑪莉歐, 進擊的巨人選項按鈕的變數
game=tk.IntVar()
# 指定tkinter模組的字串物件num，是txtNum文字欄的變數
num=tk.IntVar()  

SalesGameAry=[]     # 此串列記錄遊戲名稱
SalesNumAry=[]      # 此串列記錄遊戲銷售量   

# 建立lblGame標籤
lblGame = tk.Label(win, text='遊戲', padx=10, pady=8)
lblGame.grid(row=0, column=0)
gameAry=['火影忍者', '航海王', '人中之龍', '瑪莉歐', '進擊的巨人']
# 建立遊戲選項按鈕，有火影忍者, 航海王, 人中之龍, 瑪莉歐, 進擊的巨人選項，代表變數為game
for i in range(len(gameAry)):   
    tk.Radiobutton(win, text=gameAry[i], variable=game,
                   value=i).grid(row=0, column=(1+i))
# 預設 '人中之龍' 選項被選取
game.set(2)

# 建立銷售量lblNum標籤
lblNum = tk.Label(win, text='銷售量', padx=10, pady=8)
lblNum.grid(row=1, column=0)
# 建立txtNum文字欄，此文字欄代表變數為num
txtNum = tk.Entry(win, width=10, textvariable=num)
txtNum.grid(row=1, column=1)

# 建立btnOk按鈕，按下此鈕會執行fnOk函式
btnOk = tk.Button(win, text='確定', command=fnOk )
btnOk.grid(row=2, column=0)
win.mainloop()

