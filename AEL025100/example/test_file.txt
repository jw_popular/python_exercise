After registration participants will receive pre-course questionnaire which will be used by the 
trainer to learn about participants' teaching backgrounds and to assess their exact needs. 

Before the beginning of the course a basic reading list will be suggested to participants 
to prepare for the training. 

Participants will also be asked to prepare a presentation about themselves, 
their professional context and their culture. 

The presentation will be presented on the first day of the course to facilitate networking opportunities. 

Participants will receive information about the country they are going to visit in order to prepare them for 
their cultural experience.
