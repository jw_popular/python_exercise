# python3
import configparser

# python2
#import Configparser

import sys
import configparser
import os

# 創建對象
conf = configparser.ConfigParser()
curpath = os.path.dirname(os.path.realpath(__file__))
cfgpath = os.path.join(curpath, 'test.ini')

def loadINI():
    # 讀取INI
    conf.read(cfgpath, encoding='utf-8')
    # 取得所有sections
    sections = conf.sections()
    #
    print("----------------------------------------------------")
    print(conf["section1"])
    print(list(conf["section1"]))
    print(list(conf["section1"].values()))
    print(list(conf["section1"].items()))
    print(conf["section1"]["var1"])
    try:
        print(conf["section1"]["var4"])
    except KeyError:
        print("Key Error")
        pass
    except Exception:
        print("Unknown Error")
        pass
    print(conf["section1"].get("var2"))
    print(conf["section1"]["var1"].__class__)
    print(conf["section1"]["var2"].__class__)
    print(conf["section1"]["var3"].__class__)
    print(conf["section1"].getint("var1").__class__)

    print(dict(conf["section1"]))
    print(dict(conf))
    print({k: dict(v) for k, v in conf.items()})

    print("----------------------------------------------------")

    # 取得某section之所有items，返回格式為list
    items = conf.items('section1')

    #print(type(sections))
    #print(type(items))
    return ([sections, items])

def addSection(secTion):
    conf.add_section(secTion)

def addItem(secTion, strItem, val):
    conf.set(secTion, strItem, val)

def removeSection(secTion):
    conf.remove_section(secTion)

def removeItem(secTion, strItem):
    conf.remove_option(secTion, strItem)
 
iniContent = loadINI()
print('Sections= ', iniContent[0])
print('Items= ', iniContent[1])
 
#input("按下任意鍵結束")
if "Test" not in iniContent[0]:
    addSection("Test")
addItem("Test", "T1", "t1")
addItem("Test", "T2", "123")
addItem("Test", "T3", "Ftt")
if "Test1" not in iniContent[0]:
    addSection("Test1")
addItem("Test1", "T1", "t1")
addItem("Test1", "T2", "123")
addItem("Test1", "T3", "Ftt")
if "Test2" not in iniContent[0]:
    addSection("Test2")
addItem("Test2", "T1", "t1")
addItem("Test2", "T2", "124")
addItem("Test2", "T3", "Ftt")

if "Test1" in iniContent[0]:
    removeSection("Test1")

removeItem("Test2", "T1")

conf.write(open(cfgpath, "w"))
#conf.write(open(cfgpath, "a"))


##########################################################
newINI = os.path.join(curpath, 'cfg.ini')

# 實例化一個 ConfigParser 類的實例
config = configparser.ConfigParser()
config["basic"] = {"Host": "127.0.0.1",
                   "Port": "8888",
                   "Username": "satori"}

config["thread"] = {}
config["thread"]["name"] = "my_thread1"
config["thread"]["num"] = "3"

with open(newINI, "w", encoding="utf-8") as f:
    config.write(f)
##########################################################
