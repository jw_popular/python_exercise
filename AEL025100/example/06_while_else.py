
i=5
j=5
k=5
l=5

while(i>0):
    print(i, end = ' ')
    i-=1
else:
    print("done1")

print("-------------")

while(j>0):
    if(j==3):
        break
    print(j, end = ' ')
    j-=1
else:
    print("done2")

print("-------------")

while(k>0):
    if(k==2):
        k-=1
        continue
    print(k, end = ' ')
    k-=1
else:
    print("done3")

print("-------------")

while(l>0):
    print(l, end = ' ')
    l-=1
    if(l==2):
        continue
else:
    print("done4")

print("-------------")