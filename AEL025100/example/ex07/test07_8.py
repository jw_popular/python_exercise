def pcStore(kind, brand, mobile = "none"):
    #顯示3C產品的相關資訊
    print(f"\n您已選擇了 {kind} 類別。")
    if mobile == "none":
        print(f"在 {kind} 類別中您選擇了 {brand} 品牌。")
    else:
        print(f"在 {kind} 類別中您選擇了 {brand} 品牌，支援 {mobile}。")
    print(f"\n {kind} 可以使您生活更便利 !")
    
kind = input("請問要購買 筆電、桌機、平板或手機 哪一類的3C產品? ")
brand = input("請問是喜歡 華碩、宏碁、蘋果、或小米 哪種品牌 ? ")
if kind == "手機" or kind == "平板":
    mobile = input("請問要支援5G、4G或3G ? ")
    pcStore(kind, brand, mobile)
else:
    pcStore(kind, brand)
pcStore(mobile = "4G", brand = "華碩", kind = "手機")
pcStore("筆電", brand = "宏碁")
