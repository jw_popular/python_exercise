import random
noUsed = [1]
no = 1
groups = ["黑熊", "藍鵲", "梅花鹿", "山羊"]
count = 0
print("歡迎參加夏令營活動")
name = input("請輸入您的姓名 (輸入 q 結束) ? ")
while name != 'q' and count < 50:
    while no in noUsed:
        no = random.randint(1,50)
    print(f"{name},你的編號是{no}")
    noUsed.append(no)
    group = random.choice(groups)
    print(f"您編入 {group} 隊")
    count += 1
    name = input("請輸入您的姓名 (輸入 q 結束) ? ")
