def subpro():
    v1 = 31
    v3 = 33
    print('----- 變數(全域,區域) -----')
    print(f'v1 = {v1}, v2 = {v2}, v3 = {v3}')
    print()

v1 = 100
v2 = 200
print('----- 變數(全域) -----')
print(f'v1 = {v1}, v2 = {v2}')
print()
subpro()
print('----- 變數(全域) -----')
print(f'v1 = {v1}, v2 = {v2}')
#print(f'v1 = {V1}, v2 = {v2}, v3 = {v3}')  # 錯誤敘述
