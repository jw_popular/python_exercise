from random import randint
ans = randint(1, 6)
times = 1
print("在 1~6 整數之間猜測一個數字，最多猜三次。")
while times <= 3:
    guess = int(input("請猜一個整數 ? "))
    if guess > ans:
        print("錯誤！所猜的數字 太大了。")
    elif guess < ans:
        print("錯誤！所猜的數字 太小了。")
    else:
        print("答對了！")
        break
    times += 1


