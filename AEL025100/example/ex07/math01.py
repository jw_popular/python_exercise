import math as M
r = 100
a = r*r*M.pi
l = 2*r*M.pi
print(f'半徑為{r}的圓, 面積為{a:.2f}, 圓周長為{l:.2f}')
