
str = "Apple,Orange,Lemon,Banana"
lst = ["Python", "Java", "C++", "Pascal", "Basic"]
score1 = [20,-10,45,-5,5,15,-30,30]
score2 = [20,-10,45,-5,5,15,-30,30]

arr1 = str.split(",")
print(arr1)

st1 = ':'.join(arr1)
print(st1)

score2.sort()
print(score2)

score2.reverse()
print(score2)

sc1 = sorted(score1, reverse=False)
sc2 = sorted(score1, reverse=True)
print(score1,"-->",sc1,"-->",sc2)
