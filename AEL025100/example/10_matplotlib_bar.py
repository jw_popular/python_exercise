
import matplotlib.pyplot as plt

x1 = [10, 5, 20, 15, 22, 34]
y1 = [0, 13, 4, 25, 30, 4]
x2 = [-5, 5, -10, 15, 0, 35]
y2 = [20, -15, 5, 25, -20, 10]

# For Chinese font
font = {'family' : 'DFKai-SB'}
plt.rc('font', **font)

'''
label: Have to call legend()
'''
plt.bar(x1, y1, color='purple', label = "圖表測試")

# plt.bar(x2, y2, color='orange', label = "圖表test")
plt.bar(x1, y2, bottom=y1, color='orange', label = "圖表test")

plt.title("測試")

plt.xlabel("year年")
plt.ylabel("數量Quantity")

plt.xlim(-20, 50) # x-coord range
plt.ylim(-25, 100) # y-coord range

plt.legend() # for label 
plt.grid(True)
plt.show()

