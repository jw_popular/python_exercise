import tkinter as tk

win = tk.Tk()

win.title('測試視窗')

#win.geometry('600x600')

lbl01 = tk.Label(win, text='Label Title 中文', bg='pink', font=('DFKai-SB',16))
lbl02 = tk.Label(win, text='123 Label Title 中文', bg='yellow', fg='red', font=('DFKai-SB',24))
lbl03 = tk.Label(win, text='中文 123 中文', width=15, height=2, padx=20, pady=5, bg='lightgray', fg='blue', font=('DFKai-SB',24))
lbl04 = tk.Label(win, text='Label Title 中文', bg='pink', font=('DFKai-SB',16))

lbl01.grid(row=0, column=2)
lbl04.grid(row=0, column=5)
lbl02.grid(row=3, column=1)
lbl03.grid(row=2, column=4)

win.mainloop()