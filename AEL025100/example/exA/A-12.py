# -*- coding: utf-8 -*-
def count_fee(age, taipei):
    r = 10
    if age >= 6 and taipei == True:
        r = 30
    elif age >= 6 and taipei == False:
        if age <= 18:
            r = 50
        else:
            r = 80
    return r
