# -*- coding: utf-8 -*-
def reverse_word (str1):
    length = len(str1) - 1
    str2 = ''
    while length >= 0:
        str2 += str1[length]
        length -= 1
    return str2
print (reverse_word('!olleH'))
