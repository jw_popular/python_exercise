# -*- coding: utf-8 -*-
import os
def read_line(fileName):
    if os.path.isfile(fileName):
        with open(fileName, 'r') as f:
            return f.readline()
    else:
        return '檔案不存在'
