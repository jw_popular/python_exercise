import sys
try:
	fRead = open("read.txt", 'r')
	fWrite = open("write.txt" , 'w+')
except IOError:
    print('讀取檔案時產生錯誤')
else:
	i = 1
	for str1 in fRead:
		print(str1.rstrip())
		fWrite.write("第 " + str(i) + " 行: " + str1)
		i = i + 1
	fRead.close()
	fWrite.close()
