def div(n1, n2):
    try:
        res = n1 / n2
        print(f'{n1} / {n2} = {res}')
    except Exception as e:
        print('錯誤類型 :', end =' ')
        print(e)
    finally:
        print('執行 finally: 敘述\n')
    
div(8, 0)
div(8, 5)