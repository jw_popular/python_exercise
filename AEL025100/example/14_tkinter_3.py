import tkinter as tk

def fnHello():
    lblShow['text']="Hello World!"

def fnClear():
    lblShow['text']="---"

win = tk.Tk()

win.title('Button 範例')

win.geometry('600x600')

lblShow = tk.Label(win, text='', width=20, bg='pink', font=('DFKai-SB',16))

btnHello = tk.Button(win, text='Hello', command=fnHello, underline=2, font=('DFKai-SB',16))
btnClear = tk.Button(win, text='Clear', command=fnClear, font=('DFKai-SB',16))


lblShow.grid(row=0, column=1)
btnHello.grid(row=2, column=0)
btnClear.grid(row=2, column=2)

win.mainloop()