x=8
y=3
i = (y==3) and (x<3)
j = (x+y==11) or (y>8)
k = not((x<=y) and (x>y) or ('A'>'C'))
print(f'i={i}, j={j}, k={k}')