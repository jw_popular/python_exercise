def get_root(x, y):
    if x >= 0:
        root = x ** (1 / y)
    else:
        if x % 2 == 0:
            root = "虛數"
        else:
            root = -(-x) ** (1 / y)
    return root

print(get_root(2,2))
print(get_root(-2,2))
print(get_root(2,3))