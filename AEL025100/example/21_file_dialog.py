import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename(initialdir="./data/", title="Open Pairing Key file", filetypes=(("PK Key", "*.E33"),("All File", "*.*")))

print(file_path)