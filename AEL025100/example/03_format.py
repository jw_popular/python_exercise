
s1 = 1234.5678

s2 = "abCDEfgh"

s3 = 'price'

s4 = 1000*1000

print("[" + format(s1, '.2f') + "]")

print("|" + format(s1, '10.2f') + "|")

print("(" + format(s1, '>10.2f') + ")")

print("[" + format(s1, '<10.2f') + "]")

print("[" + format(s1, ">10f") + "]")

print("/" + format(s1, ">20.3f") + "/")
print("/" + format(s1, ">010,.3f") + "/")

print("[" + format(s2, ">10s") + "]")

print("[" + format(s3, '^7s') + "]")

print("[" + format(s4, ',') + "]")
print("[" + format(s4, '#o') + "]")
print("[" + format(s4, '#x') + "]")

# After Python 3.6

x, y = 10, 15

print(f'{x} + {y} = {x+y}')

name = "julius"

print(f'My name is {name.capitalize()}')

