
import matplotlib.pyplot as plt

x1 = [10, 5, 20, 15, 22, 34]
y1 = [0, 13, 4, 25, 30, 4]
x2 = [-5, 5, -10, 15, 0, 35]
y2 = [20, -15, 5, 25, -20, 10]

# For Chinese font
font = {'family' : 'DFKai-SB'}
plt.rc('font', **font)

'''
lw or linewidth
ls or linestyle: "-", "--", "-.", ":"
label: Have to call legend()
marker: ".", "o", "*", "v", "^", "<", ">", "s", "p", "h", "H", "d", "D", "+", "x"
ms or markersize
'''
#plt.plot(x, y, color='red', lw = 2.0, ls = "-.", label = "matplotlib test", marker = "s", markersize = 10)
plt.plot(x1, y1, color='purple', lw = 2.0, ls = "-.", label = "圖表測試", marker = "s", markersize = 10)
plt.plot(x2, y2, color='orange', lw = 1.5, ls = "--", label = "圖表test", marker = "d", markersize = 5)

plt.title("測試")

plt.xlabel("year年")
plt.ylabel("數量Quantity")

plt.xlim(-50, 50) # x-coord range
plt.ylim(-50, 50) # y-coord range

plt.legend() # for label 
plt.show()

