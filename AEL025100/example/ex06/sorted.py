animal = ['dog','cat','monkey','fox','tiger']
data = sorted(animal, reverse = False)
print(f'animal = {animal}') # animal=['dog','cat','monkey','fox','tiger']
print(f'data = {data}')    # data = ['cat','dog','fox','monkey','tiger']
