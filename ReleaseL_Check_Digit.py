

input_str = ["1111111111111111", "12B6F50000654321", "ABCDEF1212345678"]

scale = 16 ## equals to hexadecimal

num_of_digits = 12

######
def calc_ReleaseL(inputStr):
    print(inputStr)
    if len(inputStr)!=16:
        return -1

    int_ch = []
    for ich in inputStr:
        int_ch.append(int(ich, scale))
    #print(int_ch)

    temp = []
    for i in range(len(int_ch)):
        if i==0:
            for j in range(4):
                temp.append(int_ch[i])
        else:
            temp[0] ^= int_ch[i]
            if i<8:
                temp[1] ^= int_ch[i]
            if i not in (4, 5, 6, 7, 12, 13, 14, 15):
                temp[2] ^= int_ch[i]
            if i not in (2, 3, 6, 7, 10, 11, 14, 15):
                temp[3] ^= int_ch[i]

    #print(temp)   

    c = []
    for iTemp in temp:
        strTemp = bin(iTemp)[2:].zfill(4)
        #print(strTemp)
        c.append(int(strTemp[0],2) ^ int(strTemp[1],2) ^ int(strTemp[2],2) ^ int(strTemp[3],2))

    #print(c)

    check_digit = (c[3]<<3) + (c[2]<<2) + (c[1]<<1) + c[0]

    print("%X"%check_digit)
            
######


# main codes

for iStr in input_str:
    if calc_ReleaseL(iStr)==-1:
        print("Error Input")
    else:
        #print(bin(int(iStr, scale))[2:].zfill(len(iStr)*4))
        pass

