# pip install ReportLab

from reportlab.pdfgen import canvas

# help(canvas.Canvas) 

c=canvas.Canvas('HelloWorld.pdf')

# 設置頁面大小
c.setPageSize((595,842))

# 設置字體
c.setFont('Helvetica',80)

print(c._pagesize, c._fontname, c._fontsize)

# 在畫布上書寫，參數包括起點坐標和文本內容
c.drawString(50, 421, 'Hello,World!')

# 關閉當前頁並翻頁，繼續繪製下一頁
c.showPage()

c.save()