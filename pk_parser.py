
import sys
import os

import tkinter as tk
from tkinter import messagebox as msgbox
from tkinter import filedialog
from tkinter import font

import struct
import time

def setWindow(win):
    win.title("PK Key File Parsing")
    win.resizable(False, False)
    screen_width = win.winfo_screenwidth()
    screen_height = win.winfo_screenheight()
    x_cordinate = int((screen_width/2) - (window_width/2))
    y_cordinate = int((screen_height/2) - (window_height/2))
    win.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))

def button_event():
    # print(type(mylistbox.curselection()))
    lst = listbox.curselection()
    print(type(lst))
    print(lst)
    for i in range(len(lst)):
        print(listbox.get(lst[i]))

def listbox_event(event):
    object = event.widget
    # print(type(object.curselection()))
    print(object.curselection())
    index = object.curselection()[0]
    lblProdID.configure(text=object.get(index))

def load_file():
    file_path = filedialog.askopenfilename(initialdir="./data/", title="Open Pairing Key file", filetypes=(("PK Key", "*.E33"),("All File", "*.*")))
    return file_path


def check_file(file):
    if os.path.isfile(file):
        return True
        #msgbox.showinfo("Info", "File is exist! (%s)"%(file))
        #print("File is exist!")
    else:
        #msgbox.showerror("Error", "File is Not exist! (%s)"%(file))
        return False

def parsing_process():
    rawKey = load_file()

    if not check_file(rawKey):
        msgbox.showerror("Error", "File is Not exist! (%s)"%(rawKey))
        exit(1)

    for s in rawKey.split(sep='/'):
        #print("{0} {1}".format(s, s.find("csc")))
        if s.find("E33")>=0:
            strFileName = s.split(sep='.')[0]
            break

    #print(strFileName)
    lstField = strFileName.split(sep='_')

    print("==============================================================")
    print("\tSTBProvID:", lstField[1])
    print("\tCASNMin-CASNMax: %s"%(lstField[2]))
    print("\tMarket Segment ID: %s"%(lstField[3]))
    print("\tOperator Name:", lstField[4])
    print("==============================================================")
    
    lblList[0].configure(text=lstField[1])
    strCasn = lstField[2].split(sep="-")
    lblList[1].configure(text=strCasn[0])
    lblList[2].configure(text=strCasn[1])
    lblList[3].configure(text=lstField[3])
    lblList[4].configure(text=lstField[4])
    
    #print(struct.calcsize('H'))
    #print(struct.calcsize('I'))
    #print(struct.calcsize('HI'))
    parse_len = os.path.getsize(rawKey)

    with open(rawKey, "rb") as fp:
        (ref_ssv_len,) = struct.unpack("!I", fp.read(4)) # '!' us using to reverse byte, means 0x0020 -> 0x2000 
        (ref_pk_len,) = struct.unpack("!I", fp.read(4))
        print("\tSSV Length: 0x%08X (%d)"%(ref_ssv_len, ref_ssv_len))
        print("\tPairing Key Length: 0x%08X (%d)"%(ref_pk_len, ref_pk_len))

        lblList[5].configure(text="0x%08X (%d)"%(ref_ssv_len, ref_ssv_len))
        lblList[6].configure(text="0x%08X (%d)"%(ref_pk_len, ref_pk_len))
        
        print("==============================================================")
        fp.seek(0)
        
        keyCnt = 0

        while(parse_len>0):
            parse_len-=4
            (ssv_len,) = struct.unpack("!I", fp.read(4)) # '!' us using to reverse byte, means 0x0020 -> 0x2000 

            parse_len-=4
            (pk_len,) = struct.unpack("!I", fp.read(4))

            parse_len-=4
            (ca_sn,) = struct.unpack("!I", fp.read(4))
            print("\tCASN: 0x%08X (%d)"%(ca_sn, ca_sn))
            listbox.insert(keyCnt, "0x%08X (%d)"%(ca_sn, ca_sn))
            keyCnt += 1

            key_body_len = ssv_len-4-4

            parse_len-=key_body_len
            data = fp.read(key_body_len)
            
            str_val=""
            cnt=0
            for c in range(key_body_len):
                if cnt==0:
                    str_val="\t\tKBody: {0}".format("0x%02X"%(int(data[c])&0xFF))
                elif cnt!=0 and cnt%16==0:
                    str_val="{0}\n\t\t{2}{1}".format(str_val,"0x%02X"%(int(data[c])&0xFF),"       ")
                else:
                    str_val="{0} {1}".format(str_val,"0x%02X"%(int(data[c])&0xFF))
                cnt+=1
                        
            parse_len-=2
            (pk_crc,) = struct.unpack("!H", fp.read(2))    


    print("==============================================================")
    print("%.3f Seconds"%(time.time()-t_start))
    print("==============================================================")

# ----------------main--------------------
t_start = time.time()
# ----------------------------------------

window_width = 640
window_height = 480

rawKey = ""

root = tk.Tk()

setWindow(root)

#root.withdraw()

frameLabel = tk.Frame(master=root, bg="blue")
frameLabel.place(x=0, y=0, width=200, height=250)

frameContent = tk.Frame(master=root, bg="green")
frameContent.place(x=200, y=0, width=240, height=250)

frameButton = tk.Frame(master=root, bg="purple")
frameButton.place(x=440, y=0, width=200, height=250)

frame2 = tk.Frame(master=root)
frame2.place(x=0, y=250, width=window_width, height=230)

scrollbar = tk.Scrollbar(frame2)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

# selectmode= tk.BROWSE | tk.SINGLE | tk.MULTIPLE | tk.EXTENDED
listbox = tk.Listbox(frame2, selectmode=tk.EXTENDED, yscrollcommand=scrollbar.set, font=('DFKai-SB',14))

#for i in range(100):
#    listbox.insert(i, "#"+str(i))

listbox.bind("<<ListboxSelect>>", listbox_event)
listbox.pack(fill=tk.BOTH)

scrollbar.config(command=listbox.yview)

lblName = ["Prod ID", "CASN Min", "CASN Max", "Segment ID", "Operator Name", "SSV Length", "PK Length"]
lblTitleList = []
lblList = []
lblCnt = 0

for i in lblName:
    lblList.append(tk.Label(frameContent, font=('DFKai-SB',14)))
    lblList[lblCnt].grid(row=0, column=lblCnt)
    lblList[lblCnt].place(x=10, y=5+5*lblCnt+30*lblCnt, width=220, height=30)
    lblList[lblCnt].configure(text="-")

    lblTitleList.append(tk.Label(frameLabel, font=('DFKai-SB',14)))
    lblTitleList[lblCnt].grid(row=0, column=lblCnt)
    lblTitleList[lblCnt].place(x=10, y=5+5*lblCnt+30*lblCnt, width=180, height=30)
    lblTitleList[lblCnt].configure(text=lblName[lblCnt])
    lblCnt+=1

butLoad = tk.Button(frameButton, text='Load File', command=parsing_process, font=('DFKai-SB',14))
butLoad.grid(row=0, column=0)
butLoad.place(x=25, y=20, width=150, height=30)

butSel = tk.Button(frameButton, text='Selected', command=button_event, font=('DFKai-SB',14))
butSel.grid(row=1, column=0)
butSel.place(x=25, y=70, width=150, height=30)

# ----------------------------------------

root.mainloop()

