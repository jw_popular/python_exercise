#import win32com
import wmi

# create a WMI object
c = wmi.WMI()

# iterate over the physical disks
for disk in c.Win32_DiskDrive():
    # check if the disk is a physical disk (not a virtual disk)
    if "PHYSICALDRIVE" in disk.DeviceID:
        # print the serial number
        print("Serial Number:", disk.SerialNumber)
