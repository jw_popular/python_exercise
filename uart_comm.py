# python3 -m pip install serial pyserial

import serial
import serial.tools.list_ports
import time

# import serial.tools.list_ports as port_list
# ports = list(port_list.comports())
# for p in ports:
#     print (p)

ports = serial.tools.list_ports.comports()

for port, desc, hwid in sorted(ports):
    print(f'{port}: {desc} [{hwid}]')

serialPort = serial.Serial(port="COM5", baudrate=9600, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

serialString = ""

bytesvalue = b'\xFA\xF1\x02\0C'

# serialPort.write(b"Hello, World!")
serialPort.write(bytesvalue)

while 1:
    # Wait until there is data waiting in the serial buffer
    if serialPort.in_waiting > 0:

        # Read data out of the buffer until a carraige return / new line is found
        serialString = serialPort.readline()

        # Print the contents of the serial data
        try:
            print(serialString.decode("Ascii"))
        except:
            pass
        
serialPort.close()
